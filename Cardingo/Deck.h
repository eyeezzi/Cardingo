//
//  Deck.h
//  Cardingo
//
//  Created by Uzziah ignatius Eyee on 10/7/15.
//  Copyright (c) 2015 Uzziah ignatius Eyee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

// add a Card to the top or bottom of the deck
- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;

// gets a random Card form the Deck
- (Card *)drawRandomCard;

@end
