//
//  PlayingCardDeck.m
//  Cardingo
//
//  Created by Uzziah ignatius Eyee on 10/7/15.
//  Copyright (c) 2015 Uzziah ignatius Eyee. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        // create 52 PlayingCards and add them to the PlayingCardDeck
        for (NSString *suit in [PlayingCard validSuits]) {
            for (NSUInteger rank = 1; rank <= [PlayingCard maxRank]; rank++) {
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card];
            }
        }
    }
    
    return self;
}

@end
