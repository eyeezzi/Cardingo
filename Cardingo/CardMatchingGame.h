//
//  CardMatchingGame.h
//  Cardingo
//
//  Created by Uzziah ignatius Eyee on 1/2/16.
//  Copyright (c) 2016 Uzziah ignatius Eyee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"
#import "Card.h"

@interface CardMatchingGame : NSObject

@property (nonatomic, readonly) NSInteger score;

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck *)deck;
- (Card *)cardAtIndex:(NSUInteger)index;
- (void)chooseCardAtIndex:(NSUInteger)index;

@end
