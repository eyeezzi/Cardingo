//
//  Card.h
//  Cardingo
//
//  Created by Uzziah ignatius Eyee on 10/7/15.
//  Copyright (c) 2015 Uzziah ignatius Eyee. All rights reserved.
//

#import <Foundation/Foundation.h>
//@import Foundation; //new syntax

@interface Card : NSObject

@property (strong, nonatomic) NSString* contents;
@property (nonatomic, getter=isChosen) BOOL chosen;
@property (nonatomic, getter=isMatched) BOOL matched;

// Takes an array of Cards, returns a score depending on how the
// cards in the array match <this> card.
- (int)match:(NSArray *)otherCards;

@end
