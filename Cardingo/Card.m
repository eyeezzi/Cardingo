//
//  Card.m
//  Cardingo
//
//  Created by Uzziah ignatius Eyee on 10/7/15.
//  Copyright (c) 2015 Uzziah ignatius Eyee. All rights reserved.
//

#import "Card.h"

@implementation Card

- (int)match:(NSArray *)otherCards;
{
    int score = 0;
    
    // give a score of 1 if any card in the array matches this card.
    
    for (Card *card in otherCards) {
        if ([card.contents isEqualToString:self.contents]) {
            score = 1;
        }
    }
    
    return score;
}

@end
